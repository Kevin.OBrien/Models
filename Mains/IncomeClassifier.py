import sys
import numpy as np
import pandas as pd
import IncomeClassifierData as Data

rootDir = '/home/kevin/Documents/Models/'
sys.path.append(rootDir)
from MLP_TF import MLP

learningRate = 0.01
dropoutRate = 0.25
numEpochs = 10000
batchSize = 1000
printStep = 100

numClasses = 2

train = pd.read_csv(rootDir + 'Data/Income/incomeTrain.data', names=Data.columns, skipinitialspace=True)
test = pd.read_csv(rootDir + 'Data/Income/incomeTest.data', names=Data.columns, skipinitialspace=True)
full = pd.concat([train, test])
full = Data.create(full)

numTotal = full.shape[0]
numTrain = int(numTotal * 0.8)
numTest = numTotal - numTrain

train = full[:numTrain]
test = full[numTrain:]
trainInputs = train.iloc[:, :-numClasses]
trainLabels = train.iloc[:, -numClasses:]
testInputs = test.iloc[:, :-numClasses]
testLabels = test.iloc[:, -numClasses:]

numFeatures = trainInputs.shape[1]

MLP = MLP([numFeatures, 16, 16, numClasses])

numTrainBatches = numTrain // batchSize
numTestBatches = numTest // batchSize


def test():
    testLoss, testAcc = 0.0, 0.0
    for batch in range(numTestBatches):
        batchInputs = testInputs[batch * batchSize: (batch + 1) * batchSize]
        batchLabels = testLabels[batch * batchSize: (batch + 1) * batchSize]
        MLP.setBatch(batchInputs, batchLabels)
        batchLoss, batchAcc = MLP.run(['loss', 'accuracy'])
        testLoss += batchLoss
        testAcc += batchAcc
    testLoss /= numTestBatches
    testAcc /= numTestBatches
    return testLoss, testAcc


for epoch in range(1, numEpochs + 1):
    epochLoss, epochAcc = 0.0, 0.0
    for batch in range(numTrainBatches):
        batchInputs = trainInputs[batch * batchSize: (batch + 1) * batchSize]
        batchLabels = trainLabels[batch * batchSize: (batch + 1) * batchSize]
        MLP.setBatch(batchInputs, batchLabels, learningRate, dropoutRate)
        _, batchLoss, batchAcc = MLP.run(['train', 'loss', 'accuracy'])
        epochLoss += batchLoss
        epochAcc += batchAcc
    epochLoss /= numTrainBatches
    epochAcc /= numTrainBatches
    if epoch == 1 or epoch % printStep == 0:
        print("EPOCH: {} / {}".format(epoch, numEpochs))
        testLoss, testAcc = test()
        print("LOSS:  {}\t{}".format("%.4f" % epochLoss, "%.4f" % testLoss))
        print("ACC:   {}%\t{}%\n".format("%.2f" % epochAcc, "%.2f" % testAcc))
