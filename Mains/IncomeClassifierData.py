import numpy as np
import pandas as pd

columns = [
    'age',              # continuous int
    'workclass',        # Private, Self-emp-not-inc, Self-emp-inc, Federal-gov, Local-gov, State-gov, Without-pay, Never-worked
    'fnlwgt',           # continuous int
    'education',        # Bachelors, Some-college, 11th, HS-grad, Prof-school, Assoc-acdm, Assoc-voc, 9th, 7th-8th, 12th, Masters, 1st-4th, 10th, Doctorate, 5th-6th, Preschool
    'education_num',    # continuous int
    'marital_status',   # Married-civ-spouse, Divorced, Never-married, Separated, Widowed, Married-spouse-absent, Married-AF-spouse
    'occupation',       # Tech-support, Craft-repair, Other-service, Sales, Exec-managerial, Prof-specialty, Handlers-cleaners, Machine-op-inspct, Adm-clerical, Farming-fishing, Transport-moving, Priv-house-serv, Protective-serv, Armed-Forces
    'relationship',     # Wife, Own-child, Husband, Not-in-family, Other-relative, Unmarried
    'race',             # White, Asian-Pac-Islander, Amer-Indian-Eskimo, Other, Black
    'sex',              # Female, Male
    'capital_gain',     # continuous int
    'capital_loss',     # continuous int
    'hours_per_week',   # continuous int
    'native_country',   # United-States, Cambodia, England, Puerto-Rico, Canada, Germany, Outlying-US(Guam-USVI-etc), India, Japan, Greece, South, China, Cuba, Iran, Honduras, Philippines, Italy, Poland, Jamaica, Vietnam, Mexico, Portugal, Ireland, France, Dominican-Republic, Laos, Ecuador, Taiwan, Haiti, Columbia, Hungary, Guatemala, Nicaragua, Scotland, Thailand, Yugoslavia, El-Salvador, Trinadad&Tobago, Peru, Hong, Holand-Netherlands
    'income',           # >50K, <=50K
]


def create(original):
    original.replace('?', np.NaN, inplace=True)
    original.dropna(inplace=True)
    original.reset_index(inplace=True)

    agesBins = pd.cut(original.age, bins=list(range(15, 91, 5)))
    fnlwgtBins = pd.cut(original.fnlwgt, bins=list(range(-1, 1500000, 50000)))
    education_numBins = pd.cut(original.education_num, bins=list(range(-1, 17, 4)))
    capital_gainBins = pd.cut(original.capital_gain, bins=list(range(-1, 100000, 10000)))
    capital_lossBins = pd.cut(original.capital_loss, bins=list(range(-1, 4500, 500)))
    hours_per_weekBins = pd.cut(original.hours_per_week, bins=list(range(-1, 100, 10)))

    label = original.income.replace(['>50K.', '<=50K.'], ['>50K', '<=50K'])

    return pd.concat([
        pd.get_dummies(agesBins),
        pd.get_dummies(original.workclass),
        pd.get_dummies(fnlwgtBins),
        pd.get_dummies(original.education),
        pd.get_dummies(education_numBins),
        pd.get_dummies(original.marital_status),
        pd.get_dummies(original.occupation),
        pd.get_dummies(original.relationship),
        pd.get_dummies(original.race),
        pd.get_dummies(original.sex),
        pd.get_dummies(capital_gainBins),
        pd.get_dummies(capital_lossBins),
        pd.get_dummies(hours_per_weekBins),
        pd.get_dummies(original.native_country),
        pd.get_dummies(label)
    ], axis=1, ignore_index=True)
