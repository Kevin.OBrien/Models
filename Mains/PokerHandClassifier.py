import sys
import numpy as np
import pandas as pd
rootDir = '/home/kevin/Documents/Models/'
sys.path.append(rootDir)
from MLP_TF import MLP

learningRate = 0.1
dropoutRate = 0.0
numEpochs = 100
batchSize = 250
printStep = 1

numClasses = 10
numSuits = 4
numRanks = 13
handSize = 5
numFeatures = handSize * (numSuits + numRanks)


def oneHot(length, number):
    oneHot = [0.0] * length
    oneHot[number] = 1.0
    return oneHot


def batchToOneHots(batchInputs, batchLabels):
    oneHotInputs, oneHotLabels = [], []
    for row in range(len(batchInputs)):
        oneHotRow = []
        for feature in range(len(batchInputs[row])):
            if feature % 2 == 0:    # SUIT
                oneHotRow += oneHot(numSuits, batchInputs[row][feature] - 1)
            else:                   # RANK
                oneHotRow += oneHot(numRanks, batchInputs[row][feature] - 1)
        oneHotInputs.append(oneHotRow)
        oneHotLabels.append(oneHot(numClasses, batchLabels[row][0]))
    return oneHotInputs, oneHotLabels


# (25010, 11)
train = pd.read_csv(rootDir + 'Data/PokerHands/poker-hand-training-true.data', header=None)
# (1000000, 11)
test = pd.read_csv(rootDir + 'Data/PokerHands/poker-hand-testing.data', header=None)

trainInputs = train.iloc[:, :-1]
trainLabels = train.iloc[:, -1:]

testInputs = test.iloc[:, :-1]
testLabels = test.iloc[:, -1:]

numTrain = trainInputs.shape[0]
numTest = testInputs.shape[0]

MLP = MLP([numFeatures, 18, 10, numClasses])

numTrainBatches = numTrain // batchSize
numTestBatches = numTest // batchSize


def test():
    testLoss, testAcc = 0.0, 0.0
    for batch in range(numTestBatches):
        batchInputs = np.array(testInputs[batch * batchSize: (batch + 1) * batchSize])
        batchLabels = np.array(testLabels[batch * batchSize: (batch + 1) * batchSize])
        batchInputs, batchLabels = batchToOneHots(batchInputs, batchLabels)
        MLP.setBatch(batchInputs, batchLabels)
        batchLoss, batchAcc = MLP.run(['loss', 'accuracy'])
        testLoss += batchLoss
        testAcc += batchAcc
    testLoss /= numTestBatches
    testAcc /= numTestBatches
    return testLoss, testAcc


for epoch in range(1, numEpochs + 1):
    epochLoss, epochAcc = 0.0, 0.0
    for batch in range(numTrainBatches):
        batchInputs = np.array(trainInputs[batch * batchSize: (batch + 1) * batchSize])
        batchLabels = np.array(trainLabels[batch * batchSize: (batch + 1) * batchSize])
        batchInputs, batchLabels = batchToOneHots(batchInputs, batchLabels)
        MLP.setBatch(batchInputs, batchLabels, learningRate, dropoutRate)
        _, batchLoss, batchAcc = MLP.run(['train', 'loss', 'accuracy'])
        epochLoss += batchLoss
        epochAcc += batchAcc
    epochLoss /= numTrainBatches
    epochAcc /= numTrainBatches
    if epoch == 1 or epoch % printStep == 0:
        print("EPOCH: {} / {}".format(epoch, numEpochs))
        testLoss, testAcc = test()
        print("LOSS:  {}\t{}".format("%.4f" % epochLoss, "%.4f" % testLoss))
        print("ACC:   {}%\t{}%\n".format("%.2f" % epochAcc, "%.2f" % testAcc))
