import tensorflow as tf


class MLP:
    """Currently just a Softmax classifier."""

    def __init__(self, layers):
        self.layers = layers
        self.numLayers = len(layers)

        self.inputs = tf.placeholder(tf.float32, [None, layers[0]])
        self.labels = tf.placeholder(tf.int32, [None, layers[-1]])
        self.learningRate = tf.placeholder(tf.float32, [])
        self.dropoutRate = tf.placeholder(tf.float32, [])

        self.__buildNetwork()
        self.__buildTensorFlowGraph()

        self.session = tf.Session()
        self.session.run(tf.global_variables_initializer())
        self.session.graph.finalize()

    def __buildNetwork(self):
        self.weights, self.biases = [], []
        for i in range(self.numLayers - 1):
            self.weights.append(tf.Variable(tf.random_normal([self.layers[i], self.layers[i + 1]])))
            self.biases.append(tf.Variable(tf.zeros(self.layers[i + 1])))

    def __buildTensorFlowGraph(self):
        self.logits, self.outputs = self.__forwardPass()
        self.loss = self.__calculateLoss()
        self.accuracy = self.__calculateAccuracy()
        self.train = tf.train.AdamOptimizer(self.learningRate).minimize(self.loss)

    def __forwardPass(self):
        layer = self.inputs
        for i in range(self.numLayers - 1):
            layer = tf.matmul(layer, self.weights[i])
            layer = tf.add(layer, self.biases[i])
            if i != self.numLayers - 2:
                layer = tf.sigmoid(layer)
                layer = tf.nn.dropout(layer, 1.0 - self.dropoutRate)
            else:
                return layer, tf.nn.softmax(layer)

    def __calculateLoss(self):
        return tf.reduce_mean(
            tf.nn.softmax_cross_entropy_with_logits_v2(
                logits=self.logits, labels=self.labels
            )
        )

    def __calculateAccuracy(self):
        correctIndices = tf.cast(tf.equal(tf.argmax(self.outputs, axis=-1), tf.argmax(self.labels, axis=-1)), tf.float32)
        correctFraction = tf.reduce_mean(correctIndices)
        return correctFraction * 100

    def setBatch(self, inputs, labels, learningRate=0.0, dropoutRate=0.0):
        self.batchDict = {
            self.inputs: inputs,
            self.labels: labels,
            self.learningRate: learningRate,
            self.dropoutRate: dropoutRate
        }

    def run(self, operations):
        if type(operations) is not list:
            operations = [operations]

        ops = []
        for op in operations:
            if op == 'train':
                ops.append(self.train)
            if op == 'loss':
                ops.append(self.loss)
            if op == 'accuracy':
                ops.append(self.accuracy)
            if op == 'inputs':
                ops.append(self.inputs)
            if op == 'labels':
                ops.append(self.labels)
            if op == 'logits':
                ops.append(self.logits)
            if op == 'outputs':
                ops.append(self.outputs)

        return self.session.run(ops, self.batchDict)
