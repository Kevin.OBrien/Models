# ################################################################
# TO DO
# ################################################################
#
# Only build graph when train method called
# Automatically get 'num_unrollings'
# Automatically get 'num_inputs' & 'num_outputs'
# Single timestep for inference
# Train, inference, forecast methods
# Save best model during training
# Only run pad & mask when needed / Check to pad sequences automatically
# If padded, only return needed elements
# Find better way to create masks
# Add momentum?
# Check if dropout is still applied when dropout_rate = 0 (inefficient)
# If batch size = 1, format output
# If num_inputs / num_outputs = 1, format placeholders/output
# Ensure num_unrollings % seq_length == 0
# Eliminate unstacking & stacking of 'rnn_outputs' and 'outputs'
# Allow activation to be specified
# Allow GD algo to be specified
# Allow softmax classification
# Test masked vs unmasked loss
# Stateful & stateless
# Many-to-Many & Many-to-One


# ################################################################
# DONE
# ################################################################
#
# Allow variable length sequences
# Only add L2/dropout when l2_alpha/dropout_rate > 0


# ################################################################
# SHAPES
# ################################################################
#
# inputs:               [batch_size, num_unrollings, num_inputs]
# labels:               [batch_size, num_unrollings, num_outputs]
#
# output_weights:       [hidden_sizes[-1], num_outputs]
# output_biases:        [num_outputs]
#
# rnn_outputs:          [batch_size, num_unrollings, hidden_sizes[-1]]
#                       [num_unrollings * [batch_size, hidden_sizes[-1]]]
#
# outputs:              [num_unrollings * [batch_size, num_outputs]]
#                       [batch_size, num_unrollings, num_outputs]
#
# predictions:          [batch_size, num_unrollings, num_outputs]
#
# masks:                [batch_size, num_unrollings, num_outputs]
# masked_labels:        [batch_size, num_unrollings, num_outputs]
# masked_predictions    [batch_size, num_unrollings, num_outputs]


import tensorflow as tf
import tensorflow.contrib.rnn as tf_rnn
import pandas as pd
import os


class RNN:

    def __init__(self, num_inputs, num_outputs, hidden_sizes, num_unrollings=1, cell='RNN'):
        self.root_dir = os.path.dirname(os.path.abspath(__file__))
        self.ckpt_dir = os.path.join(self.root_dir, 'SavedModels')

        # ARCHITECTURE
        cell_types = {'RNN': tf_rnn.BasicRNNCell, 'LSTM': tf_rnn.BasicLSTMCell, 'GRU': tf_rnn.GRUCell}
        self.cell_name = cell.upper()
        self.cell_type = cell_types[cell.upper()]
        self.num_inputs = num_inputs
        self.num_outputs = num_outputs
        self.hidden_sizes = hidden_sizes
        self.num_unrollings = num_unrollings

        # HYPERPARAMETER PLACEHOLDERS
        self.learning_rate = tf.placeholder(tf.float32, shape=[], name='learning_rate')
        self.dropout_rate = tf.placeholder(tf.float32, shape=[], name='dropout_rate')
        self.l2_alpha = tf.placeholder(tf.float32, shape=[], name='l2_alpha')

        # DATA PLACEHOLDERS
        self.inputs = tf.placeholder(tf.float32, shape=[None, num_unrollings, num_inputs], name='inputs')
        self.labels = tf.placeholder(tf.float32, shape=[None, num_unrollings, num_outputs], name='labels')
        self.lengths = tf.placeholder(tf.int32, shape=[None, num_inputs], name='lengths')
        self.batch_size = tf.placeholder(tf.int32, shape=[], name='batch_size')

        # DENSE OUTPUT LAYER HYPERPARAMETERS
        self.output_weights = tf.Variable(tf.random_normal(shape=[hidden_sizes[-1], num_outputs]), name='output_weights')
        self.output_biases = tf.Variable(tf.random_normal(shape=[num_outputs]), name='output_biases')

        # STACKED RNN LAYERS
        self.rnn = self.__build_rnn()

        # TENSORFLOW DYNAMIC RUNTIME GRAPH
        self.session = tf.Session()
        self.optimizer = tf.train.AdamOptimizer(self.learning_rate)
        self.__build_tensorflow_graph()
        self.saver = tf.train.Saver()
        self.session.run(tf.global_variables_initializer())
        self.session.graph.finalize()   # Avoids memory leak through dynamic creation of duplicate operations

    # ################################################################
    # PRIVATE METHODS
    # ################################################################

    def __build_rnn(self):
        layers = []
        for layer_size in self.hidden_sizes:
            cell = self.cell_type(layer_size)
            cell = tf_rnn.DropoutWrapper(cell, output_keep_prob=(1.0 - self.dropout_rate))
            layers.append(cell)
        stacked_layers = tf_rnn.MultiRNNCell(layers)
        return stacked_layers

    def __build_tensorflow_graph(self):
        self.zero_state = self.rnn.zero_state(self.batch_size, tf.float32)
        self.reset_state()
        rnn_outputs, self.state = tf.nn.dynamic_rnn(self.rnn, self.inputs, initial_state=self.state)

        rnn_outputs = tf.unstack(rnn_outputs, axis=1)
        outputs = [tf.add(tf.matmul(output, self.output_weights), self.output_biases) for output in rnn_outputs]
        outputs = tf.stack(outputs, axis=1)

        self.predictions = tf.nn.relu(outputs, name='predictions')
        self.loss = self.__calculateLoss()
        self.loss += tf.cond(self.l2_alpha > 0.0, self.__calculate_l2_regularization, lambda: 0.0)
        self.optimize = self.optimizer.minimize(self.loss)

    def __calculateLoss(self):
        masks = tf.sequence_mask(lengths=self.lengths, maxlen=self.num_unrollings)
        masks = tf.transpose(masks, [0, 2, 1])

        masked_labels = tf.boolean_mask(self.labels, masks)
        masked_predictions = tf.boolean_mask(self.predictions, masks)

        loss = tf.losses.mean_squared_error(labels=masked_labels, predictions=masked_predictions)
        # loss = tf.losses.mean_squared_error(labels=self.labels, predictions=self.predictions)
        return loss

    def __calculate_l2_regularization(self):
        l2 = self.l2_alpha * sum(
            tf.nn.l2_loss(weight)
            for weight in tf.trainable_variables()
            if not ("noreg" in weight.name or "Bias" in weight.name)
        )
        return l2

    def __pad_data(self, data):
        max_length = 0
        for df in data.values():
            max_length = max(df.shape[0], max_length)
        max_length = max_length if max_length % self.num_unrollings == 0 else max_length + self.num_unrollings
        for id, df in data.items():
            padded_rows = pd.DataFrame({col: [0] * (max_length - df.shape[0]) for col in df.columns})
            data[id] = df.append(padded_rows)
        return data

    def __save(self, path=None, model_name=None):
        path = self.ckpt_dir if not path else path
        model_name = self.cell_name if not model_name else model_name
        model_name += '.ckpt'
        self.saver.save(self.session, os.path.join(path, model_name))

    def __restore(self, path=None, model_name=None):
        path = self.ckpt_dir if not path else path
        model_name = self.cell_name if not model_name else model_name
        model_name += '.ckpt'
        self.saver.restore(self.session, os.path.join(path, model_name))

    # ################################################################
    # PUBLIC METHODS
    # ################################################################

    def reset_state(self):
        self.state = self.zero_state

    def train(self, data, epochs, learning_rate, dropout_rate=0.0, l2_alpha=0.0):
        data = self.__pad_data(data)
