import pandas as pd
import os

from RNN import RNN

wd = os.path.dirname(os.path.realpath(__file__))
input_file = 'cashflow.csv'

data = pd.read_csv(os.path.join(wd, input_file))
data.rename(columns={'Client_ID': 'ID', 'Yearly_income': 'Inc', 'Yearly_expends': 'Exp'}, inplace=True)
data['ID'] = data['ID'].apply(lambda str: int(str.split('_')[-1]))
ids = data['ID'].unique()
data.set_index(['ID', 'Date'], inplace=True)
data = data.iloc[:, -2:]
data[['Inc_Label', 'Exp_Label']] = data[['Inc', 'Exp']].shift(-1)

data_dict = {id: data.loc[id].iloc[:-1] for id in ids}

norm_values = {}
for id, df in data_dict.items():
    min, max = df.min(), df.max()
    data_dict[id] = (df - min) / (max - min)
    norm_values[id] = {}
    norm_values[id]['min'] = min
    norm_values[id]['max'] = max

model = RNN(num_inputs=2, num_outputs=2, hidden_sizes=[100, 100], num_unrollings=86, cell='RNN')

model.train(data_dict, 1, 0.01)
