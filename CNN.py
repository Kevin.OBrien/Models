import os

import tensorflow as tf


class CNN():

    def __init__(
        self,
        totalPixels,
        rowPixels,
        colPixels,
        numClasses
    ):

        self.rawInputs = tf.placeholder(
            tf.float32, [None, totalPixels]
        )

        self.inputs = tf.reshape(
            self.rawInputs, [-1, rowPixels, colPixels, 1]
        )

        self.labels = tf.placeholder(
            tf.float32, [None, numClasses]
        )

        self.numClasses = numClasses

        convolutionalOutputs = self.__passThroughConvolutionalLayers(self.inputs)
        self.denseOutputs, self.predictions = self.__passThroughFullyConnectedLayers(convolutionalOutputs)
        self.loss = self.__calculateLoss(self.denseOutputs, self.labels)
        self.accuracy = self.__calculateAccuracy(self.predictions, self.labels)

        self.learningRate = tf.placeholder(tf.float32, [])
        self.SGD = tf.train.AdamOptimizer(self.learningRate).minimize(self.loss)

        self.session = tf.Session()
        self.saver = tf.train.Saver()
        self.session.run(tf.global_variables_initializer())

    def __passThroughConvolutionalLayers(self, inputs):
        layer1 = self.__createConvolutionalLayer(
            inputs, 1, 32, [5, 5], [2, 2]
        )

        layer2 = self.__createConvolutionalLayer(
            layer1, 32, 64, [5, 5], [2, 2]
        )

        convolutionalOutputs = tf.reshape(layer2, [-1, 7 * 7 * 64])
        return convolutionalOutputs

    def __createConvolutionalLayer(self, inputs, numChannels, numFilters, filterShape, poolShape):
        filter = [filterShape[0], filterShape[1], numChannels, numFilters]
        weights = tf.Variable(tf.random_normal(filter))
        bias = tf.Variable(tf.random_normal([numFilters]))
        convolution = tf.nn.conv2d(inputs, weights, [1, 1, 1, 1], padding='SAME')
        convolution = tf.add(convolution, bias)
        nonLinear = tf.nn.relu(convolution)
        poolSize = [1, poolShape[0], poolShape[1], 1]
        poolStrides = [1, 2, 2, 1]
        output = tf.nn.max_pool(nonLinear, ksize=poolSize, strides=poolStrides, padding='SAME')
        return output

    def __passThroughFullyConnectedLayers(self, inputs):
        weights1 = tf.Variable(tf.random_normal([7 * 7 * 64, 1000]))
        bias1 = tf.Variable(tf.random_normal([1000]))
        layer1 = tf.matmul(inputs, weights1) + bias1
        layer1 = tf.nn.relu(layer1)
        weights2 = tf.Variable(tf.random_normal([1000, self.numClasses]))
        bias2 = tf.Variable(tf.random_normal([self.numClasses]))
        layer2 = tf.matmul(layer1, weights2) + bias2
        probs = tf.nn.softmax(layer2)
        return layer2, probs

    def __calculateLoss(self, outputs, labels):
        loss = tf.nn.softmax_cross_entropy_with_logits_v2(logits=outputs, labels=labels)
        loss = tf.reduce_mean(loss)
        return loss

    def __calculateAccuracy(self, predictions, labels):
        correctPredictions = tf.equal(tf.argmax(predictions, -1), tf.argmax(labels, -1))
        accuracy = tf.reduce_mean(tf.cast(correctPredictions, tf.float32))
        return accuracy

    def setBatch(self, inputs, labels=None, learningRate=0.0):
        if labels is None:
            labels = [[0] * self.numClasses]
        self.batchDict = {
            self.rawInputs: inputs,
            self.labels: labels,
            self.learningRate: learningRate
        }

    def train(self):
        self.session.run(self.SGD, self.batchDict)

    def get(self, operations):
        ops = []
        for op in operations:
            if op == 'labels':
                ops.append(tf.stack(self.labels, axis=1))
            if op == 'predictions':
                ops.append(self.predictions)
            if op == 'loss':
                ops.append(self.loss)
            if op == 'accuracy':
                ops.append(self.accuracy)
        return self.session.run(ops, self.batchDict)

    def save(self, modelName="CNN"):
        modelName += '.ckpt'
        dir = os.path.dirname(os.path.realpath(__file__)) + '/SavedModels/'
        self.saver.save(self.session, dir + modelName)

    def restore(self, modelName="CNN"):
        modelName += '.ckpt'
        dir = os.path.dirname(os.path.realpath(__file__)) + '/SavedModels/'
        self.saver.restore(self.session, dir + modelName)
